//projekt UMIM
#define ENCODER_USE_INTERRUPTS                          // Definicja dla biblioteki <encoder.h>
#define ENCODER_OPTIMIZE_INTERRUPTS                     // Definicja dla biblioteki <encoder.h>
#include <Encoder.h>
#include <ArduinoJson.h>                                // Używam do komunikacji z LabView.

//definicja wejść
#define PendulumEncoderA 2                              //Pin A enkodera wahadła
#define PendulumEncoderB 3                              //Pin B enkodera wahadła
#define PWM 10                                          //Pin mostka H do sterowania prędkością silnika
#define left 9                                          //Pin mostka H do lewych obrotów
#define right 11                                        //Pin mostka H do prawych obrotów

//dla enkodera1 (wykorzystuje 2 przerwania (równie dobrze działał na jednym ale przelutowałem płytkę i zostały już 2, enkoder silnika nie jest brany pod uwagę)
volatile long PendulumEncoderTicks = 0;                 //Impulsy enkodera wahadła
volatile long PendulumTicks = 0;
volatile double anglePendulum = 0;                      //Zmienna przechowująca kąt wychylenia
volatile double anglePendulumSend = 0;                  //Zmienna przechowująca kąt wychelia wysyłany do LabView
volatile float velocityPendulum = 0;                    //Prędkość wahadła
volatile float anglePendulum_prev = 0;                  //Poprzednie położenie wahadła - kąt
volatile float anglePendulum_post = 0;                  //Zeszłe położenie wahadła - Kąt

//parametry wahadła
//opcjonalnie do liczenia energii ale i tak dobrałem wartości na eksperymentalnie 
double l = 0.116;                                       // długość wahadła [m]
double m = 0.012;                                       // waga wahadla [kg]
double g = 9.82;                                        // przyspieszenie ziemskie [m/s]
double Energy;                                          // energia wahadła
double reqEnergy = 15;                                  // Oczekiwana energia dla warunku w funkcji swing 

//przerwania timera
volatile int tcnt2 = 131;                               // Zmienna do przerwania timera, żeby się wykonywało co 1ms
volatile int t = 0;                                     // Zmienna pomocnicza do estymacji prędkosci
float motor_input = 0;                                  // Zmienna wykorzystywana do przeliczenia prędkości silnika

//regulator PID
volatile float e_proportional = 0;                      // błąd członu proporcjonalnego
volatile float e_derivative = 0;                        // błąd członu różniczkującego
volatile float e_integral = 0;                          // błąd członu całkujacego
double kp = 120;                                        // Wzmocnienie proporcjonalne
double kd = 0.2;                                        // wzmocnienie różniczki
double ki = 0.2;                                        // wzmocnienie całki
float e = 0;                                            // Uchyb
int INPUT_MAX = 230;                                    // maxymalna moc silnika dla PID, nie 255 ponieważ układ świrował nieziemsko
int INPUT_SLOW_1= 180;  
int INPUT_SLOW_2= 100;                                  // Zwolnienie prędkosci swingu dla pewnego zakresu
int INPUT_TMP;                                          // Zmienna pomocnicza do przyśpieszania/zwalniania wahadła
int INPUT_MAX_SWING = 220;                              // maksymalna prędkość silnika podczas swingu
bool firstPID = false;                                  // pomocnicza zmienna do resetowania wartości błędów PID'a
int setPoint = 97;                                      // punkt pozycji górnej


//timer dla komunikacji
unsigned long interval = 10;                            // czas przerwy
unsigned long previousMillis=0;                         // zmienna pomocnicza, zapisuje poprzedni stan czasu

Encoder pendulumEnc(PendulumEncoderA,PendulumEncoderB); //zmienna enkodera

//zmienne pomocnicze
bool startApplication = false;


// obsługa przerwania (trochę przydługa ale starałem się nie korzystać z biblioteki PID_v1, skorzystanie z niej znacznie skróci to przerwanie
ISR(TIMER2_OVF_vect) {  

  TCNT2 = tcnt2;  // przeładowanie timera
  t++;
  PendulumEncoderTicks = pendulumEnc.read() / 4;    //odczyt pozycji enkodera (przez 4)
  
  //Wyzerowanie licznika enkodera po wykonaniu pełnego obrotu
  if(PendulumEncoderTicks > 200) {
    PendulumEncoderTicks -= 200;
    pendulumEnc.write(PendulumEncoderTicks);
  }
  else if (PendulumEncoderTicks < -200) {
    PendulumEncoderTicks += 200;
    pendulumEnc.write(PendulumEncoderTicks);
  }
  //Koniec zerowania

  //Zmiana zakresu działania regulatora PID
  if(setPoint  - 3 <= abs(PendulumEncoderTicks) && setPoint + 3  >= abs(PendulumEncoderTicks)) {
    PendulumTicks = setPoint;
    //velocityPendulum = 0;
    Stop();
  }
  else {   
    PendulumTicks = PendulumEncoderTicks;
  }

  // przeliczenie wartości położenia wahadła send- wysyłane do LV
  anglePendulumSend = PendulumEncoderTicks * 0.031416;
  anglePendulum = PendulumTicks * 0.031416;
  // estymacja prędkosci
  if (t == 1){
    anglePendulum_prev = anglePendulum;
  }
  else if (t == 20){
    anglePendulum_post = anglePendulum;  
  }
  else if (t == 21){
    velocityPendulum = (anglePendulum_post - anglePendulum_prev)*50; 

    e_proportional = anglePendulum - setPoint  * 0.0314;
    e_derivative = velocityPendulum - 0;
    e_integral = e_integral + e_proportional;
    
    t = 0;  //wyzerowanie zmiennej pomocniczej
  }
}

//REGULATOR PID
void rPID() {
  if(firstPID == false) {
    e_proportional = 0;
    e_derivative = 0;
    e_integral = 0;
    velocityPendulum = 0;
    firstPID = true;
  }
  
   e = - (kp*e_proportional + kd*e_derivative + ki*e_integral); // wyliczenie uchybu 

  //printData("pid");
  motor_input = INPUT_MAX / 5.3 * e; // przelicenie na prędkość silnika
  motor_input = (int)motor_input;    // rzutowanie zmiennej na inta
  // Zabezpieczenie zeby nie przekroczyć wartości maksymalnej silnika
  if (motor_input >= INPUT_MAX){
    motor_input = INPUT_MAX;
  }
  else if (motor_input <= -INPUT_MAX){
    motor_input = -INPUT_MAX;
  }
  setMotorSpeed(motor_input);
 //KONIEC REGULACJI
}

// funkcja pozwalajaca na ustawienie wahadła w górnej pozycji
void swingUp() {
        firstPID = false; // jeśli wahadło spadnie należy ustawić firstPID na 0, wyzerować błędy regulacji
        computeEnergy(PendulumEncoderTicks,anglePendulumSend); // Obliczenie energii wahadła
        //printData("swing");

        //warunek obrotu w lewo
       if((velocityPendulum < 0 && Energy < reqEnergy) || (velocityPendulum >= 0 && Energy >= reqEnergy)){
       doRight();
       analogWrite(PWM,INPUT_TMP);  //silnik na 235PWM poniweaż na maxie świrował nieziemsko
       delay(100);
       //Serial.println("prawo");
       }
       //warunek obrotu w prawo
       else if((velocityPendulum >= 0 && Energy < reqEnergy) ||(velocityPendulum < 0 && Energy >= reqEnergy)){
       doLeft();
       analogWrite(PWM,INPUT_TMP);
       delay(100);
       //Serial.println("lewo");
       }
}

//funkcja przeliczająca energie
double computeEnergy(double y,double z){
     //Energy = 0.5 * J * y * y + m * g * l * (1 + cos(z)); // energia wahadla
     Energy = 0.010773 * y * y + 0.9536 * (1 + cos(z)); //0.010773*y*y+1.0536*(1+cos(z));
     return Energy;
}

//ustawienie prędkości silnika 
void setMotorSpeed(int EngineSpeed) {
  if (EngineSpeed < 0)
  {
    doLeft();
    EngineSpeed = -EngineSpeed;       //zmiana prędkości na wartośc dodatnią 
    analogWrite(PWM,EngineSpeed);
  }
  else if (EngineSpeed > 0) {
    doRight();
    analogWrite(PWM,EngineSpeed);
  }
  else {
    Stop();
  }
}

//lewy obrót silnika
void doLeft() {
    digitalWrite(left, LOW);
    digitalWrite(right, HIGH);
}

//silnik w prawo
void doRight() {
    digitalWrite(right, LOW);
    digitalWrite(left, HIGH);
}

//zatrzymanie silnika
void Stop() {
  digitalWrite(left,LOW);
  digitalWrite(right,LOW);
  analogWrite(PWM,0);
}

void printData(char x[]) {
        Serial.print(x);
        Serial.print("\t");
        Serial.print(PendulumEncoderTicks);
        Serial.print("\t");
        Serial.print(velocityPendulum);
        Serial.print("\t");
        Serial.print(Energy);
        Serial.print("\t");
        Serial.print(anglePendulumSend);
        Serial.print("\n");
}

// wysłanie ramki po serialu
bool Send() {
  const size_t BUFFER_SIZE =
      JSON_OBJECT_SIZE(3)    // 3 elementy
      + JSON_OBJECT_SIZE(1)  // płytka
      + JSON_OBJECT_SIZE(1)  // czas
      + JSON_OBJECT_SIZE(6);  // dane

  DynamicJsonBuffer jsonBuffer(BUFFER_SIZE);
  JsonObject& root = jsonBuffer.createObject();
  
  root["device"] = "Arduino UNO";
  root["time"] = millis();
  JsonArray& data = root.createNestedArray("data");
  data.add(double_with_n_digits(kp, 2));
  data.add(double_with_n_digits(kd, 2));
  data.add(double_with_n_digits(ki, 2));
  data.add(double_with_n_digits(e, 2));
  data.add(double_with_n_digits(anglePendulumSend,2));
  data.add(double_with_n_digits(setPoint,2));
  
  
  root.printTo(Serial);
  Serial.print("\n"); //koniec lini
  return true;
}

// funkcja odbierająca ramkę z serialu
bool Read() {
  const size_t BUFFER_SIZE =
      JSON_OBJECT_SIZE(3)    // 3 elementy
      + JSON_OBJECT_SIZE(1)  // device
      + JSON_OBJECT_SIZE(1)  // czas
      + JSON_OBJECT_SIZE(5);  // dane

  DynamicJsonBuffer jsonBuffer(BUFFER_SIZE);

  JsonObject& root = jsonBuffer.parseObject(Serial);

  if (!root.success()) {
    //Serial.println("\nJSON parsing failed!");
    return false;
  }

  kp = root["data"][0];
  kd = root["data"][1];
  ki = root["data"][2];
  setPoint = (int)root["data"][3];
  startApplication = (bool)root["data"][4];

  return true;
}

void setup(){
    Serial.begin(9600);
    pendulumEnc.write(0); // Zero Encoder value.

   //ustawienie przerwania timera2 co 1ms (dokumentacja atmega328p)
   TIMSK2 &= ~(1<<TOIE2);
   TCCR2A &= ~((1<<WGM21) | (1<<WGM20));
   TCCR2B &= ~(1<<WGM22);
   ASSR &= ~(1<<AS2);
   TIMSK2 &= ~(1<<OCIE2A);
   TCCR2B |= (1<<CS22)  | (1<<CS20);
   TCCR2B &= ~(1<<CS21);
   TCNT2 = tcnt2;
   TIMSK2 |= (1<<TOIE2);
}

void loop(){
     unsigned long currentMillis = millis();
     //Serial.println(PendulumEncoderTicks);
     
     if(startApplication == true) {
           //warunki zakresu dizałania PID'a i swingu
           if(abs(anglePendulumSend) > ((setPoint * 0.0314) - 1.64)  && abs(anglePendulumSend) < ((setPoint * 0.0314) + 1.64 )) INPUT_TMP = INPUT_SLOW_1;
           if(abs(anglePendulumSend) > ((setPoint * 0.0314) - 0.14)  && abs(anglePendulumSend) < ((setPoint * 0.0314) + 1.14 )) INPUT_TMP = INPUT_SLOW_2;
           else INPUT_TMP = INPUT_MAX_SWING;
           
           if(abs(anglePendulumSend) > ((setPoint * 0.0314) - 0.3 ) && abs(anglePendulumSend) < ((setPoint * 0.0314) + 0.3 )) rPID(); //2.8344,3.1944
           else swingUp();
     }
     else Stop();
     //komunikacja z LabView wywoływana co jakiś interwał (zdefiniowany na początku programu)
     if ((unsigned long)(currentMillis - previousMillis) >= interval) {
          Send(); //wysłanie ramki
          Read(); // czytanie ramki
          previousMillis = millis();
     }
}
